title: Mujeres Liderando Proyectos Sustentables - Noviembre 2014
categories:
  - Eventos
date: 2014-12-26 02:35:08
---

<a href="/2015/01/m1.jpg"><img class="img-fluid aligncenter size-full wp-image-294" src="/2015/01/m1.jpg" alt="m1" width="540" height="540" /></a>
<!--more-->

<a href="/2015/01/m2.jpg"><img class="img-fluid aligncenter size-full wp-image-295" src="/2015/01/m2.jpg" alt="m2" width="512" height="651" /></a>

<a href="/2015/01/m3.jpg"><img class="img-fluid aligncenter size-full wp-image-296" src="/2015/01/m3.jpg" alt="m3" width="529" height="654" /></a>

&nbsp;
<div class="guias" style="margin: 15px; padding: 15px;">
<h3>Mayor Información de la RED Y DEL 2do Encuentro:</h3>
<a href="/2015/01/doc-principal-.pdf">doc-principal.pdf</a>

<a href="/2015/01/Presentación-y-encuentro-noviembre-3.pdf">Presentación-y-encuentro-noviembre-3.pdf</a>

</div>