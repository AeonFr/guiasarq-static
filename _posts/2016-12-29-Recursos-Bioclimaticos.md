---
layout: page
title: Recursos Bioclimáticos
categories: [lo_que_hacemos]
date: 2015-02-26 03:11:17
---


<p>La <a href="http://es.wikipedia.org/wiki/Arquitectura_bioclim%C3%A1tica" target="_blank">arquitectura bioclimática</a> consiste en el diseño de edificios teniendo en cuenta las condiciones climáticas, aprovechando los recursos disponibles (sol, vegetación, lluvia, vientos) para disminuir los impactos ambientales, intentando reducir los consumos de energía.</p>
<ul>
<li>Módulos captadores de energía solar.</li>
<li>Cubiertas Verdes.</li>
</ul>