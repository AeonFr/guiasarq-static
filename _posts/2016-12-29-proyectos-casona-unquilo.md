---
layout: page
title: Remodelación de la casona unquillo
categories: proyectos
date: 2014-12-26 02:30:00
thumb: /images/proy/casona-unquillo/thumb3.jpg
---

![Casona Unquillo](/images/proy/casona-unquillo/thumb3.jpg)
<!--more-->
Esta casona de comienzos del siglo pasado se encontraba deshabitada, estando obsoletas sus instalaciones  el techo bastante deteriorado. Cuenta con un sistema de canaletas, bajadas y conductos en piso para recuperar el agua de lluvia enviándola a un aljibe.

![](/images/proy/casona-unquillo/casona-1.jpg)

Actualmente se han recuperado los muros, algunos fueron demolidos lo que se trabajó con mucho cuidado para poder reciclar el ladrillo y ser reutilizado en muros a construir. Se colocaron perfiles UPN de demolición donde se demolieron muros.

![](/images/proy/casona-unquillo/casona-2.jpg)