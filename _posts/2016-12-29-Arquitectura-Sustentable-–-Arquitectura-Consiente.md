---
layout: page
title: Arquitectura Sustentable / Arquitectura Conciente - Taller Virtual
author: Arquitecto Federico Salgado
categories: [notas_de_divulgacion]
date: 2015-02-26 16:47:31
thumb: /images/arquitectura-sustentable-conciente/slide01-300x186.jpg
---

Aporte Referente: Taller Virtual -&gt; Arq. Federico Salgado

[![](/images/arquitectura-sustentable-conciente/slide01-300x186.jpg)](/images/arquitectura-sustentable-conciente/slide01.jpg)

<!--more-->

[![](/images/arquitectura-sustentable-conciente/slide02-300x212.jpg)](/images/arquitectura-sustentable-conciente/slide02.jpg)
[![](/images/arquitectura-sustentable-conciente/slide03-300x216.jpg)](/images/arquitectura-sustentable-conciente/slide03.jpg)
[![](/images/arquitectura-sustentable-conciente/slide04-300x218.jpg)](/images/arquitectura-sustentable-conciente/slide04.jpg)
[![](/images/arquitectura-sustentable-conciente/slide05-300x218.jpg)](/images/arquitectura-sustentable-conciente/slide05.jpg)
[![](/images/arquitectura-sustentable-conciente/slide06-300x217.jpg)](/images/arquitectura-sustentable-conciente/slide06.jpg)
[![](/images/arquitectura-sustentable-conciente/slide07-300x217.jpg)](/images/arquitectura-sustentable-conciente/slide07.jpg)
[![](/images/arquitectura-sustentable-conciente/slide08-300x218.jpg)](/images/arquitectura-sustentable-conciente/slide08.jpg)
[![](/images/arquitectura-sustentable-conciente/slide09-300x215.jpg)](/images/arquitectura-sustentable-conciente/slide09.jpg)
[![](/images/arquitectura-sustentable-conciente/slide10-300x216.jpg)](/images/arquitectura-sustentable-conciente/slide10.jpg)
[![](/images/arquitectura-sustentable-conciente/slide11-300x219.jpg)](/images/arquitectura-sustentable-conciente/slide11.jpg)
[![](/images/arquitectura-sustentable-conciente/slide12-300x220.jpg)](/images/arquitectura-sustentable-conciente/slide12.jpg)
[![](/images/arquitectura-sustentable-conciente/slide13-300x217.jpg)](/images/arquitectura-sustentable-conciente/slide13.jpg)
[![](/images/arquitectura-sustentable-conciente/slide14-300x215.jpg)](/images/arquitectura-sustentable-conciente/slide14.jpg)