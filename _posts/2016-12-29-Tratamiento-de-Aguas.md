---
layout: page
title: Tratamiento de Aguas
categories: [lo_que_hacemos]
date: 2014-12-30 05:08:22
---

Existen diversas formas y muchos criterios para utilizar eficientemente el agua en un edificio pero es necesario prever desde el proyecto las distintas situaciones y alternativas para no generar gastos posteriores. En regiones como Córdoba con regímenes de lluvia tan irregulares se hace necesario contar en los periodos secos con recursos de recuperación de aguas para reciclarlas o utilizarlas para riego.


<!--more-->
<div style="text-align: justify;"><a href="/2014/12/tratamiento_de_aguas.jpg">
</a> <a href="/2014/12/tratamiento_de_aguas_2.png"><img class=" size-full wp-image-223 aligncenter" src="/2014/12/tratamiento_de_aguas_2.png" alt="tratamiento_de_aguas_2" width="707" height="480" /></a></div>
<div style="text-align: center;"><em>Tratamiento de aguas.</em></div>
<div style="text-align: justify;"><a href="/2014/12/unnamed.png"><img class="aligncenter size-full wp-image-215" src="/2014/12/unnamed.png" alt="Esquema de utilización eficiente del agua en una vivienda" width="509" height="523" /></a></div>
<div style="text-align: center;"> <em>Utilización eficiente del agua en una vivienda.</em></div>

## Tratamiento de Aguas Residuales

**Instalación de un Bio-digestor para tratamiento de aguas residuales:**

En las zonas rurales, serranas o urbanas sin servicio de cloacas, es necesario realizar un tratamiento previo de las aguas servidas antes de enviarlas a la napas subterráneas para no producir la contaminación de las mismas. Un recurso simple y poco costoso es reemplazar la cámara séptica por un biodigestor. Actualmente el mercado ofrece alternativas variadas y de fácil instalación.

![Biodigestor](/images/biodigestor.jpg)