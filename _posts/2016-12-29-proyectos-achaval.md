---
layout: page
title: 'Edificio Achaval B° Observatorio'
date: 2014-12-26 02:30:00
categories: proyectos
thumb: /images/proy/achaval/1-300x225.png
---

[![1](/images/proy/achaval/1-300x225.png)](/images/proy/achaval/1.png)
[![2](/images/proy/achaval/03-300x225.png)](/images/proy/achaval/03.png)
[![3](/images/proy/achaval/04-300x225.png)](/images/proy/achaval/04.png)<!--more-->
[![4](/images/proy/achaval/06-300x225.jpg)](/images/proy/achaval/06.jpg)
[![5](/images/proy/achaval/07-169x300.jpg)](/images/proy/achaval/07.jpg)
[![6](/images/proy/achaval/08-300x275.jpg)](/images/proy/achaval/08.jpg)
[![7](/images/proy/achaval/10-169x300.jpg)](/images/proy/achaval/10.jpg)
[![8](/images/proy/achaval/11-169x300.jpg)](/images/proy/achaval/11.jpg)
[![9](/images/proy/achaval/12-300x169.jpg)](/images/proy/achaval/12.jpg)
[![10](/images/proy/achaval/13-169x300.jpg)](/images/proy/achaval/13.jpg)