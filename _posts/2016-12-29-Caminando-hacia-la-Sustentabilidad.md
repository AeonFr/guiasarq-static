---
layout: page
title: Caminando hacia la Sustentabilidad
categories: [notas_de_divulgacion]
date: 2015-02-26 03:11:17
thumb: /images/sustentabilidad.jpg
---

Actualmente se han producido importantes transformaciones para mejorar las condiciones medioambientales y el ahorro energético, tanto a nivel de los programas de funcionamiento, los requisitos de confort los aspectos constructivos y la forma de operar los edificios. La mayor parte de las innovaciones se han originado bajo el concepto de _sustentabilidad_.<span id="more-72"></span>
<!--more-->
![Sustentabilidad](/images/sustentabilidad.jpg)

En nuestro país hay mucho por hacer, ya que éste tema se ha abordado con retraso por lo que nos compromete a trabajar con urgencia y recuperar el tiempo perdido tanto en lo teórico como en la práctica Profesional.

Se debe pensar el edificio desde el inicio del proyecto con los componentes ambientales incorporados a nuestra propuesta. Esta mirada sobre la sustentabilidad del hábitat debe orientarse hacia el buen uso de los materiales, el asoleamiento, recuperar y reutilizar el agua de lluvia, separación y tratamiento de las aguas grises y las negras, incorporar las cubiertas verdes  y preservar las especies autóctonas en los espacios verdes.

Desde nuestro accionar profesional queremos difundir éstas prácticas y así lograr la conciencia de la necesidad de ser mas sustentables, no sólo en los profesionales sino a toda la sociedad  que habita éste planeta.

<div class="margin-before" style="clear: left;"></div>

![Camino](/images/camino.jpg)