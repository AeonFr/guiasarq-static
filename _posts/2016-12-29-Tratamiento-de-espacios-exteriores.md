---
layout: page
title: Tratamiento de espacios exteriores.
categories: [lo_que_hacemos]
date: 2015-02-26 01:10:34
---

<div>Un buen proyecto arquitectónico es aquel que está pensado en forma integral, teniendo en cuenta las características espaciales, morfológicas, estéticas, tecnológicas, energéticas, constructivas, etc. El entorno en el que se implanta el edificio debe no sólo ser tenido en cuenta como proveedor de datos para nuestro análisis conceptual del proyecto, sino como parte del mismo. Por eso el tratamiento del espacio exterior que circunda a nuestro proyecto es tan importante y debe tratarse y pensarse de la misma manera que el espacio interior.</div>

<!--more-->

<div>La elección y ubicación de especies vegetales, la incorporación de recursos bioclimáticos que colaboren en el ahorro energético y la elección de materiales utilizados para tratarlos, son algunas de las consideraciones que un buen proyecto debe tener.</div>
<div>Se pueden generar &#8220;ambientaciones&#8221; exteriores para captar energía solar o para protegerse de la misma, con algunos recursos bioclimáticos pensados desde el inicio del proyecto.</div>
<div>A la hora de elegir material para nuestros pisos exteriores, senderos, etc. es necesario contar con aquellos que se conocen como &#8220;pisos absorbentes&#8221; que permiten el traspaso por capilaridad de las aguas de lluvia permitiendo al suelo natural recuperar parte de lo que pierde al asentar el edificio.</div>
<div>En cuanto a la parquización, es necesario conocer cuáles son las especies que por ser originarias o autóctonas no producen deterioro al medio ambiente ya sea por exceso de consumo de agua, o por ser invasoras y enfermar el ecosistema natural.</div>
<div></div>

[![Pisos Absorventes](/images/tratamiento-de-espacios-exteriores/pisos-absorbentes-300x200.jpg)](/images/tratamiento-de-espacios-exteriores/paving-stone.jpg)
[![OLYMPUS DIGITAL CAMERA](/images/tratamiento-de-espacios-exteriores/P5210018-300x225.jpg)](/images/tratamiento-de-espacios-exteriores/P5210018.jpg)
[![OLYMPUS DIGITAL CAMERA](/images/tratamiento-de-espacios-exteriores/P5110060-300x225.jpg)](/images/tratamiento-de-espacios-exteriores/P5110060.jpg)
[![OLYMPUS DIGITAL CAMERA](/images/tratamiento-de-espacios-exteriores/cabaña-4-300x225.jpg)](/images/tratamiento-de-espacios-exteriores/cabaña-4.jpg)
