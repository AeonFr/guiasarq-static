---
layout: page
title: Recursos de captación y protección solar
categories: [lo_que_hacemos]
date: 2014-12-30 05:18:10
thumb: /images/captacion-y-proteccion-solar/Calidad-ambiental-invierno-SD.png
---

Por medio de estrategias de proyecto se pueden generar recursos de captación y protección solar logrando de esta manera una mayor eficiencia energética que permite disminuir y a veces eliminar la utilización de tecnologías de aporte energético (calefacción o aire acondicionado).

<!--more-->

[![Calidad ambiental invierno](/images/captacion-y-proteccion-solar/Calidad-ambiental-invierno-SD.png)](/images/captacion-y-proteccion-solar/Calidad-ambiental-invierno-HD.png)
[![Calidad ambiental verano](/images/captacion-y-proteccion-solar/Calidad-ambiental-verano-SD.png)](/images/captacion-y-proteccion-solar/Calidad-ambiental-verano-HD.png)
[![Grafico muro trombe invierno](/images/captacion-y-proteccion-solar/Grafico-muro-trombe-invierno-SD.png)](/images/captacion-y-proteccion-solar/Grafico-muro-trombe-invierno-HD.png)
[![Grafico muro trombe ventilacion](/images/captacion-y-proteccion-solar/Grafico-muro-trombe-ventilacion-SD.png)](/images/captacion-y-proteccion-solar/Grafico-muro-trombe-ventilacion-HD.png)
[![Grafico muro trombe verano](/images/captacion-y-proteccion-solar/Grafico-muro-trombe-verano-SD.png)](/images/captacion-y-proteccion-solar/Grafico-muro-trombe-verano-HD.png)
[![Orientacion oeste](/images/captacion-y-proteccion-solar/Orientacion-oeste-SD.png)](/images/captacion-y-proteccion-solar/Orientacion-oeste-HD.png)
[![PLANTACRITERIOS](/images/captacion-y-proteccion-solar/PLANTACRITERIOS-SD.png)](/images/captacion-y-proteccion-solar/PLANTACRITERIOS-HD.png)