---
layout: page
title: Prados de la Lagunita
categories: proyectos en_proyecto
date: 2014-12-26 02:30:30
thumb: /images/proy/prados-de-la-lagunita.jpg
---

![Prados de la lagunita](/images/proy/prados-de-la-lagunita.jpg)

<!--more-->

Desarrolla GRUPO EMAIDES Miguel Emaides Arq. Silvia Brusa Arq. Alejandra Tobar

Prados de la Lagunita, nace con la idea de crear un Barrio de Campo dentro de la ciudad; su ubicación, las visuales a las sierras, el entorno inmediato, nos llevaron a plantearnos la idea de generar un barrio distinto y distinguible dentro del ejido urbano de la ciudad de Còrdoba.

Distinto porque a pesar de contar con terrenos de dimensiones urbanas, sus asentamientos serán regulados para permitir la mayor cantidad de espacios verdes, con veredas arboladas y calles con acceso vehicular restringido para posibilitar el intercambio social entre vecinos.

Asimismo los servicios de energía eléctrica serán subterráneos permitiendo una imagen visual más agradable.

Además las viviendas se construirán con una supervisión y asesoramiento arquitectónico tendiente a lograr diseños regulados donde se priorizará el proyecto bioclimático que permita la eficiencia en el uso de los recursos energéticos y de agua.

Distinguible porque el barrio contará con ingreso de seguridad, espacios comunitarios y de servicios, espacios verdes recreativos y deportivos, además de una infraestructura de servicios pensada desde el desarrollo sustentable que contempla alumbrado público con energía solar, tratamiento de aguas residuales y de la basura.

Además el diseño de su planimetría está pensado desde la zonificación de actividades, comercios, deportes, y actividades comunitarias ubicadas de manera de no interferir con las viviendas. En cuanto a lo formal toma el agua como elemento estructurante en base a la recuperación, reserva y reutilización de la misma. El sistema será conformado por canales y acequias que rematarán en una laguna artificial, logrando de esta manera no sólo disminuir la necesidad de infraestructuras pluviales sino que además alimentará la laguna actuando como reservorio para riego y tratamiento de aguas grises. Su mantenimiento será realizado por la incorporación de especies vegetales y peces con características depuradoras.

Contempla además incorporación de bici-sendas y espacios para deportes.

El diseño paisajístico contempla el respeto al ecosistema natural existente con la incorporación de especies autóctonas y riego autosustentable.

En cuanto a la imagen de senderos y calles, se utilizarán materiales cuyo grado de permeabilidad permita que las aguas de lluvia alcancen el terreno natural y mejorar las condiciones de humedad del terreno natural.

Los propietarios contarán con el asesoramiento para sus proyectos con la idea de incorporar prácticas arquitectónicas y constructivas sustentables que irán desde el diseño bioclimático, la selección de materiales poco agresivos y con bajo costo de mantenimiento, ahorro energético en sistema de agua caliente, gestión de residuos desde la vivienda y una adecuada gestión de obra que no sea invasiva ni altere la armonía que se pretende mantener.

De esta manera Prados de la Lagunita se presenta como una nueva alternativa para la ciudad, esperando atraer habitantes con intereses similares y generar entre todos una propuesta de calidad.

##  **Hablar del urbanismo de Córdoba**

El desafío de este proyecto son sus características dadas a partir de la ubicación, el amanzanamiento, distribución de los lotes y los aspectos legales, todo esto genera una serie de condicionantes especiales, distintas y  a su vez deben ser incorporadas a una futura trama urbana de la ciudad.

Se trata de un barrio de 250 lotes aprobado en el año 1937, y que nunca se ocupó por diversos factores que tienen que ver con la ausencia de una planificación urbana coherente. La superficie total es de     m2 y su forma es un triángulo con sus lados orientados norte-sur y este-oeste y cuya hipotenusa forma parte de un camino de conexión de poco uso entre la ciudad de Córdoba y la ciudad de Malagueño.

Se encuentra en un área no consolidada al sur-oeste de la ciudad de Córdoba dentro del éjido urbano de la misma, a unos mil metros de la futura circunvalación. Su entorno actual es rural, y el acceso se complica por ser calles de tierra sin mantenimiento y usada por camiones y tractores.

El trazado aprobado consta de 7 manzanas regulares de 100x60m y 6 irregulares formadas al encontrarse la trama con la diagonal.