---
layout: page
title: Materiales Sustentables
categories: [notas_de_divulgacion]
author: Silvia Brusa
date: 2015-02-26 02:29:18
thumb: /images/materiales-sustentables.jpg
---

Existen distintos criterios cuando se habla de sustentabilidad, la visión de la misma varía de acuerdo a la problemática predominante del entorno socio-geográfico y económico del país de referencia. En países en vías de desarrollo como el nuestro la problemática económica y social está asociada al degrado del medio ambiente, esto determina que a la hora de establecer estrategias y acciones sustentables las prioridades son muy diferentes.<span id="more-62"></span>
<!--more-->
![materiales-sustentables](/images/materiales-sustentables.jpg)

Por eso consideramos que un buen criterio sustentable sería aquel que permita satisfacer los requerimientos de calidad sin comprometer el medio ambiente o comprometiéndolo en la menos medida posible.

Decimos que  un edificio es sustentable en la medida que es capaz de incorporar nuevos hábitos en todos los procesos que involucra desde el proyecto, la construcción, uso y mantenimiento.

Por este motivo es que se debe poner especial atención en seleccionar las acciones que lleven a disminuir los costos ambientales pensando también en todo el ciclo de vida de un edificio.

En relación a los Materiales a emplear, podríamos decir que una acción sustentable sería la desmaterialización, es decir la reducción en el consumo de materiales por metro cuadrado útil de construcción, ya sea mejorando el proyecto, seleccionando métodos constructivos que garanticen el desempeño adecuado con la utilización de la menor cantidad de materiales, reduciendo pérdidas y evitando necesidades de reposición de productos de baja calidad o mantenimientos costosos.

![Materiales](/images/materiales.jpg)

La selección de Materiales es una de las acciones que inciden particularmente en la conservación de recursos disponibles.

Cuando tenemos que clasificar a un material como sustentable, nos encontramos con la dificultad de aplicar un criterio único y de mayor peso sobre otro. Según qué parámetros tengamos en cuenta, un material puede ser eficiente para un determinado criterio y para otro no.

Esta dificultad es la que nos lleva a plantearnos a la hora de encarar un proyecto edilicio, la necesidad de realizar un estudio y una clasificación adecuada a nuestra realidad social, de materiales “sustentables”, posicionándonos en distintas miradas frente a los criterios existentes y la oferta actual de materiales en nuestro medio.

En el país no existe una oferta de materiales definidos como sustentables tan diversificada como en EU y Europa, pero el mercado de estos productos está creciendo de manera importante, debido, en parte, a la mayor conciencia de los consumidores sobre los problemas ambientales y/o sobre los beneficios económicos de un ahorro energético o de agua; a los programas gubernamentales a favor de una construcción sustentable, o a la popularización de sistemas de certificación o etiquetación de edificios verdes.

![Materiales (2)](/images/materiales-2.jpg)

Consideramos que un Material es sustentable a aquél cuyo proceso de extracción, manufactura, operación y disposición final tiene un impacto ambiental bajo; es económicamente viable, su fabricación implica el empleo de mano de obra local, y durante su vida útil no compromete la calidad de vida de los seres vivos que están de alguna manera en contacto directo con él, incluyendo al ser humano. La metodología que se utilizó para determinar qué tan verde es un producto de construcción es el Análisis de Ciclo de Vida, debido a que es el método de análisis más completo que hay hasta ahora.

La carencia de información y de un marco normativo suele generar confusión acerca de qué es un material sustentable, y ha propiciado un fenómeno conocido mundialmente como greenwashing, que consiste en que el mercadeo sea hecho de manera tal, que logre que sus consumidores potenciales perciban sus productos como amigables con el medio ambiente, cuando realmente no lo son.

El profesional debe conocer cuáles son las herramientas adecuadas para decidir el material a utilizar y poder justificar su uso y su costo frente al desafío de la sustentabilidad.

![De la materia prima al deshecho](/images/de-la-materia-prima-al-deshecho.jpg)