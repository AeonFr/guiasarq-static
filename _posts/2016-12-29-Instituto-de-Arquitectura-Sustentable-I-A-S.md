---
layout: page
title: Instituto de Arquitectura Sustentable (I.A.S.)
categories: [notas_de_divulgacion, IAS]
author: Adriana Mendieta
date: 2015-02-26 01:32:42
thumb: /images/manifiesto-IAS/image-1.jpg
---

Manifiesto del I.A.S. (Instituto de Arquitectura sustentable del [Colegio de Arquitectos de la Provincia de Córdoba ](http://www.colegio-arquitectos.com.ar/))
<!--more-->
[![Diapositiva 1](/images/manifiesto-IAS/image-1.jpg)](/images/manifiesto-IAS/image-1.jpg)
[![Diapositiva 2](/images/manifiesto-IAS/image-2.jpg)](/images/manifiesto-IAS/image-2.jpg)
[![Diapositiva 3](/images/manifiesto-IAS/image-3.jpg)](/images/manifiesto-IAS/image-3.jpg)
[![Diapositiva 4](/images/manifiesto-IAS/image-4.jpg)](/images/manifiesto-IAS/image-4.jpg)